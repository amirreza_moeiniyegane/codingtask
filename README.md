# Coding Task

## Overview
A restful web service for encrypting and decrypting texts using caesar cipher 

## Requirements
Java

Eclipse IDE for Enterprise Java Developers

Apache tomcat server


## Languages and Framework used in the project

Java 10 

Spring Framework 5 

JavaScript/css/html5 

bootstrap 3


## How to download
Open Git Bash, (you need to download and install [git](https://git-scm.com/downloads) if you haven't it already)

Change the current working directory to the location where you want the project to be downloaded to
by typing cd 'directory-location' Example:
```bash
cd C:/Users/YourUsername/Documents/MyProjects
```

Type git clone and paste the URL of this project as follows

```bash
git clone https://gitlab.com/amirreza_moeiniyegane/codingtask.git
```

Press Enter and wait for the project to be downloaded.

## How to run

1. Import & Run the Project:

   1. files > Open Project from File System... > direcory> select the directory that you downloaded project to.
   2. Right-click on the project in the left panel > run as > run on the server
   

## How to Configure from Scratch
Download and Install All **Requirements**
mentioned above from this list below or from anywhere that you want if you don't already have them installed.

-[Java](https://www.oracle.com/technetwork/java/javase/downloads/index.html)

-[Eclipse IDE for Enterprise Java Developers](https://www.eclipse.org/downloads/packages/release/2019-09/r/eclipse-ide-enterprise-java-developers)

-[Apache tomcat server](https://tomcat.apache.org/download-90.cgi)

1. Configure eclipse ide:
    1. open eclipse ide, go to windows > preferences > java>installed JREs
    1. select the JDK you have installed
    1. open windows > show view > servers 
    1. right-click on server windows > new > server
    1. select apache in the opened windows select **Tomcat v9.0   Server**
    1. choose "localhost" for server's Host Name, if it's not by default
    1. click finish


## Explanation for other Spring Developers

classes with "model" suffix are used for request and response, in other words, a request will be converted to a model class
before we work on it, for example, a request comes to the controller with a JSON data with it that has script and offset
it will be converted to ```EncryptModel.java``` which has 2 fields of the script and the offset.
same model classes for responses unless the response is a primitive type or an object of java standard APIs e.g. ```String```, ```List<>``` and so on.

```@RequestMappins``` are used at class levels, for all controllers.
except for ```FallBackController```  

all the controllers use ```@RestController``` annotation so ```@ResponseBody``` annotation is not used separately

exceptions are handled in another class named ```ControllerAdvice.java```, URL controllers only take care of codes and responses, not exceptions.

text files and images that are used in the views are stored in src/main/resources and will be extracted ```ResourceManager.java``` class

most of the CSS's are in the view are bootstrap and some of theme are overwritten.

objects/attributes in the view ``${}`` have been sent from ``HomeController.java`` 

the URI ``/images/imagename.jpg`` is used to load images, you can use that to load images in the view


## Test Units
unit tests are in 3 different packages, Mockito is used to simulate dependencies, and another unit test has been created for each of those dependencies
to test all the codes of Controllers and DAO's

[Assured tests](https://gitlab.com/amirreza_moeiniyegane/codingtaskassured.git) 
have been made to test URLs responses without simulating with Mockito

## How to use Assured Tests:
   1. files > Open Project from File System... > direcory> select the directory that you downloaded project to
   2. have the main project running on the server
   3. open project in the left panel > src/test/java > run or modifies tests.
   4. **BaseURI and port is been set to default localhost:8080, change them in the setUp method of test units if your baseURI and port is different**



## Project Structure

config classes are in: ``/CodingTask/src/main/java/com/codingtask/webappconfig`` or ``com.codingtask.webappconfig`` package

controlelr classes are in: `` /CodingTask/src/main/java/com/codingtask/controller ``or ``com.codingtask.controller`` package

exception classes are in: ``/CodingTask/src/main/java/com/codingtask/exception/EncryptionException.java`` or ``com.codingtask.exception`` package

model classes are in: ``/CodingTask/src/main/java/com/codingtask/model`` or ``com.codingtask.model`` package

encrypter class is in:  ``/CodingTask/src/main/java/com/codingtask/cryptography`` or ``com.codingtask.cryptography`` package

resource access class is in: ``/CodingTask/src/main/java/com/codingtask/resourceaccess`` or ``com.codingtask.resourceaccess`` package

views are in: ``/CodingTask/WebContent/WEB-INF/view``

resources are in: ``/CodingTask/src/main/resources``

test classes are in: ``/CodingTask/srs/test/java/com/codingtask``