package com.codingtask.webappconfig;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.PathMatchConfigurer;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.view.JstlView;
import org.springframework.web.servlet.view.UrlBasedViewResolver;

import com.codingtask.cryptography.StringEncrypter;
import com.codingtask.resourceaccess.ResourceManager;

@Configuration
@EnableWebMvc
@ComponentScan("com.codingtask")
public class WebApplicationConfiguration implements WebMvcConfigurer {

	@Bean
	public UrlBasedViewResolver urlBasedViewResolver() {
		UrlBasedViewResolver urlBasedViewResolver = new UrlBasedViewResolver();
		urlBasedViewResolver.setPrefix("/WEB-INF/view/");
		urlBasedViewResolver.setSuffix(".jsp");
		urlBasedViewResolver.setViewClass(JstlView.class);

		return urlBasedViewResolver;
	}

	@Bean
	public StringEncrypter stringEncrypter() {
		return new StringEncrypter();

	}

	@Bean
	public ResourceManager resourceManager() {
		return new ResourceManager();
	}

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("/resources/**").addResourceLocations("/resources/", "/WebContent/WEB-INF/");

	}

	@Override
	public void configurePathMatch(PathMatchConfigurer configurer) {
		configurer.setUseTrailingSlashMatch(true);

	}

}
