package com.codingtask.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.codingtask.cryptography.StringEncrypter;
import com.codingtask.exception.EncryptionException;
import com.codingtask.model.EncryptModel;

@RestController
@RequestMapping("/encrypt")
public class EncryptController {

	@Autowired
	private StringEncrypter stringEncrypter;

	@PostMapping
	public ResponseEntity<String> encryptText(@RequestBody EncryptModel encryptModel) {

		if (encryptModel == null) {
			throw new EncryptionException("Encrypt request is invalid");
		}

		if (encryptModel.hasErrors()) {
			throw new EncryptionException("Can not perform the encryption on an empty field.");
		}

		String enctypted = stringEncrypter.encrypt(encryptModel.getScript(), encryptModel.getOffset());

		return new ResponseEntity<String>(enctypted, HttpStatus.OK);

	}

}
