package com.codingtask.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.servlet.ModelAndView;

import com.codingtask.exception.EncryptionException;
import com.codingtask.exception.UrlNotFoundException;
import com.codingtask.model.ExceptionModel;

@RestControllerAdvice
public class ControllerAdvice {

	@ExceptionHandler
	public ResponseEntity<ExceptionModel> encryptionExceptionHandler(EncryptionException e) {

		ExceptionModel exceptionModel = new ExceptionModel(e.getMessage(), HttpStatus.NOT_ACCEPTABLE.value());

		return new ResponseEntity<ExceptionModel>(exceptionModel, HttpStatus.NOT_ACCEPTABLE);

	}

	@ExceptionHandler
	public ModelAndView urlNotFoundExceptionHandler(UrlNotFoundException e) {
		ModelAndView mav = new ModelAndView();

		mav.setViewName("ErrorView");

		mav.addObject("errorHeader", HttpStatus.NOT_FOUND.value());
		mav.addObject("errorMessage", e.getMessage());
		mav.setStatus(HttpStatus.NOT_FOUND);

		return mav;
	}

}
