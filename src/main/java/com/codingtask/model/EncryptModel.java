package com.codingtask.model;

public class EncryptModel {

	private String script;
	private int offset;

	public EncryptModel() {

	}

	public EncryptModel(String script, int shift) {
		this.script = script.trim();
		this.offset = shift;
	}

	public String getScript() {
		return script;
	}

	public void setScript(String script) {
		this.script = script.trim();
	}

	public int getOffset() {
		return offset;
	}

	public void setOffset(int shift) {
		this.offset = shift;
	}

	public boolean hasErrors() {

		if (script == null) {
			return true;
		}

		if (script.isEmpty()) {
			return true;
		}
		return false;
	}

}
