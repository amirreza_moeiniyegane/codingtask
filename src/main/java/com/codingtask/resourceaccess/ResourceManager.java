package com.codingtask.resourceaccess;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.file.Files;

import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

public class ResourceManager {

	public byte[] loadImage(String imageName) {
		
		System.out.println("ResourceManager.loadImage()");
		Resource resource = new ClassPathResource(imageName + (imageName.endsWith("png")? ".png" : ".jpg"));

		try {
			File file = resource.getFile();

			byte[] imageBytes = Files.readAllBytes(file.toPath());
			
			return imageBytes;

		} catch (Exception e) {			
			return null;

		}

	}

	public String readText(String textFile) {

		Resource resource = new ClassPathResource(textFile);

		StringBuilder stringBuilder = new StringBuilder();

		try {
			File file = resource.getFile();

			BufferedReader bufferedReader = new BufferedReader(new FileReader(file));

			bufferedReader.lines().iterator().forEachRemaining(i -> {
				stringBuilder.append(i);
			});

			bufferedReader.close();

		} catch (Exception e) {			
			return null;

		}

		return stringBuilder.toString();

	}

}
