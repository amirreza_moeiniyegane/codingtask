package com.codingtask.exception;

public class UrlNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -506935803501929085L;

	public UrlNotFoundException(String message) {
		super(message);

	}

}
