package com.codingtask.resourceaccess;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

class ResourceManagerTest {

	@InjectMocks
	private ResourceManager resourceManager;

	@BeforeEach
	void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	void testLoadImage() {
		
		assertNotNull(resourceManager.loadImage("background"));
		
		assertTrue(resourceManager.loadImage("hand-made-caesar-cipher") instanceof byte[]);

	}

	@Test
	void testReadText() {

		assertNotNull(resourceManager.readText("julius-caesar-wiki"));

		assertThat(resourceManager.readText(ArgumentMatchers.anyString()), is(nullValue()));

		assertFalse(resourceManager.readText("caesar-cipher-wiki").contains("caesar"));
		assertTrue(resourceManager.readText("caesar-cipher-wiki").toLowerCase().contains(("caesar")));

	}

}
