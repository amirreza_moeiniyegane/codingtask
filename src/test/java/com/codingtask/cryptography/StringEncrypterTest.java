package com.codingtask.cryptography;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;

class StringEncrypterTest {

	@InjectMocks
	StringEncrypter stringEncrypter;

	@BeforeEach
	void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testEncrypt() {
		assertThat(stringEncrypter.encrypt("a", 1), is("b"));
		assertThat(stringEncrypter.encrypt("florian", 3), is("ioruldq"));
		assertThat(stringEncrypter.encrypt("cristiano", 18), is("ujaklasfg"));

		assertThat(stringEncrypter.encrypt("defend the east wall of the castle", 1),
				is("efgfoe uif fbtu xbmm pg uif dbtumf"));

		assertThat(stringEncrypter.encrypt("efgfoe uif fbtu xbmm pg uif dbtumf", -1),
				is("defend the east wall of the castle"));

		assertThat(stringEncrypter.encrypt("defend the east wall Of the CASTLE", 1),
				is("efgfoe uif fbtu xbmm Pg uif DBTUMF"));

		assertThat(stringEncrypter.encrypt("visconti", -12), is("jwgqcbhw"));
		assertThat(stringEncrypter.encrypt("keyboArd", 26), is("keyboArd"));	
		
	}

}
