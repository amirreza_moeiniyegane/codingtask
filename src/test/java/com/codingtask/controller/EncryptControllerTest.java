package com.codingtask.controller;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.codingtask.cryptography.StringEncrypter;
import com.codingtask.exception.EncryptionException;
import com.codingtask.model.EncryptModel;

class EncryptControllerTest {

	@InjectMocks
	EncryptController encryptController;

	@Mock
	StringEncrypter stringEncrypter;

	private EncryptModel encryptModel;

	@BeforeEach
	void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	void testEncryptText() {
		when(stringEncrypter.encrypt((ArgumentMatchers.anyString()), ArgumentMatchers.anyInt()))
				.thenReturn("zxcasdqwerfv");

		encryptModel = new EncryptModel("test", 1);

		assertThrows(EncryptionException.class, () -> {
			encryptController.encryptText(null);
		});

		ResponseEntity<String> responseEntity = new ResponseEntity<String>("zxcasdqwerfv", HttpStatus.OK);

		assertEquals(responseEntity, encryptController.encryptText(encryptModel));

	}

}
