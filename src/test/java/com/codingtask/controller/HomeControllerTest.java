package com.codingtask.controller;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.web.servlet.ModelAndView;

import com.codingtask.resourceaccess.ResourceManager;

class HomeControllerTest {

	@InjectMocks
	private HomeController homeController;

	@Mock
	private ResourceManager resourceManager;

	@BeforeEach
	void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	void testHomePage() {

		when(resourceManager.loadImage(ArgumentMatchers.anyString())).thenReturn("randomImage".getBytes());

		when(resourceManager.readText(ArgumentMatchers.anyString())).thenReturn("randomString");

		ModelAndView mav = homeController.homePage();

		assertNotNull(mav);

		assertTrue(mav.getViewName().equals("homeView"));

		assertTrue(mav.getModelMap().containsKey("cipherDiskImage"));

		assertTrue(mav.getModelMap().get("handMadeCipherImage").equals("hand-made-caesar-cipher.jpg"));

		assertTrue(mav.getModel().get("juliusCaesarWikiHeader") != null);

	}

}
